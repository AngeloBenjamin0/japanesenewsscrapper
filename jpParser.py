from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from mainfunction import *
from csv import writer

# Grabs a url from tv asahi newspaper and exports a csv file with the sentences split by punctuation signs.
# The export csv file is meant to be imported in to Anki and parse the 1T (one unknown word) sentences with morphman.
# This is what is called Sentence Mining in Immersion language learning.

url = 'https://news.tv-asahi.co.jp/news_economy/articles/000208338.html'
request = Request(url, headers={
    'User-Agent': ''
                  ''})
page = urlopen(request)
soup = BeautifulSoup(page, 'html.parser',)

title = soup.find("h1", class_='news_title').get_text().strip()
text = soup.find("div", class_='maintext').get_text().strip()
thumbnail = soup.find('input', {'name': 'thumbnail'}).get('value').strip()
articleId = soup.find('input', {'name': 'articleId'}).get('value').strip()

# print(title,text,thumbnail,articleId)
#
fulltext = title + text
fulltext.strip(" ")
# print(fulltext)
#
# print(article_url)
#
# print(replacedText(fulltext, thumbnail))
#
# print(fulltext)
#
cutText = replacedText(fulltext, thumbnail)

# print(cutText)
lines = cutText.splitlines()

# print(lines)

thumbnail_Url = 'https://news.tv-asahi.co.jp'+thumbnail
html_img = '<img src=\"'+thumbnail_Url+'\"/>'
# print(html_img)

with open(articleId+'.csv', "w", encoding='UTF-8', newline='') as csv_file:
    # csv_file.write(cutText)
    csv_file.write('\ufeff')
    csv_writer = writer(csv_file)
    for rows in lines:
        csv_writer.writerow([rows, html_img])

# file = open(articleId+'.csv', "w")
# writer = csv.writer(file)
# writer.writerow([fulltext.encode('utf-8')])
# file.close()
